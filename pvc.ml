
Random.self_init ();;

type graph = int array array (* Every graph must be complete *)

type solution = int array

type rsolution = 
  { sol : solution
  ; mutable valor : int
  }

type pop = rsolution array


(** Random population generation, for now : crappy algorithm *)

let swap arr i j =
  let tmp = arr.(i) in
  arr.(i) <- arr.(j);
  arr.(j) <- tmp;
  ()

(* Knuth array shuffle *)
let shuffle arr =
  for i = Array.length arr - 1 downto 1 do
    let j = Random.int i in
    swap arr i j;
  done;
  ()

(* Generates a random population of `size` solutions of size n *)  
let generate_population size n =
  let pop = Array.make size { sol = Array.init n (fun x -> x); valor = 0 } in
  Array.iter (fun s -> shuffle s.sol) pop;
  pop


(** Evaluation *)

(* Computes the valor of a solution, i.e. the length of the path *)
let eval_solution g sol =
  let v = ref 0 in
  for i = 0 to Array.length sol - 2 do
    v := !v + g.(sol.(i)).(sol.(i+1))
  done;
  !v
 

(** Selection *)

(* TODO *)

(** Crossing and mutation *)

(* Position of the locus for a solution of size n *)
let cross_locus n = n / 3
  (* Random.int n *)
 
(* Crosses two solutions *)
let cross a b =
  let n = Array.length a in
  (* The result array *)
  let res = Array.make n 0 in
  (* The array of presence, to avoid duplications *)
  let pres = Array.make n false in 
  let t = cross_locus n in
  for i = 0 to t do (* We copy a in res until the locus' position *)
    res.(i) <- a.(i);
    pres.(a.(i)) <- true;
  done;
  (* then we copy all the nodes in b that are not already in res *)
  let j = ref (t+1) in 
  for i = 0 to n-1 do
    if not pres.(b.(i)) then begin 
      res.(!j) <- b.(i);
      pres.(b.(i)) <- true;
      incr j
    end
  done;
  res

(* Mutation of a solution *)
let mutate sol =
  let n = Array.length sol in
  let i = Random.int n in
  let j = Random.int n in
  swap sol i j;
  ()


    
      
    
  
